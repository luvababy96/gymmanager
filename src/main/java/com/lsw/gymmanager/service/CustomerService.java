package com.lsw.gymmanager.service;

import com.lsw.gymmanager.entity.Customer;
import com.lsw.gymmanager.model.CustomerItem;
import com.lsw.gymmanager.model.CustomerRequest;
import com.lsw.gymmanager.model.CustomerResponse;
import com.lsw.gymmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer();
        addData.setName(request.getName());
        addData.setAge(request.getAge());
        addData.setHeight(request.getHeight());
        addData.setBeforeWeight(request.getBeforeWeight());
        addData.setAfterWeight(request.getAfterWeight());
        addData.setSignUpDate(LocalDate.now());

        customerRepository.save(addData);

    }

    public List<CustomerItem> getCustomers() {
        List<Customer> originList = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for(Customer item : originList) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setName(item.getName());
            addItem.setAge(item.getAge());
            addItem.setHeight(item.getHeight());
            addItem.setBeforeWeight(item.getBeforeWeight());
            addItem.setAfterWeight(item.getAfterWeight());
            addItem.setSignUpDate(item.getSignUpDate());

            result.add(addItem);
        }

        return result;
    }
    public CustomerResponse getCustomer(long id) {
        Customer originData = customerRepository.findById(id).orElseThrow();

        CustomerResponse result = new CustomerResponse();
        result.setId(originData.getId());
        result.setName(originData.getName());
        result.setAge(originData.getAge());
        result.setHeight(originData.getHeight());
        result.setWeight("Before" + originData.getBeforeWeight() + "/" + "After" + originData.getAfterWeight());
        result.setSignUpDate(originData.getSignUpDate());

        return result;
    }

}
