package com.lsw.gymmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false,length = 20)
    private String name;
    @Column(nullable = false)
    private Float age;
    @Column(nullable = false)
    private Float height;
    @Column(nullable = false)
    private Float beforeWeight;
    @Column(nullable = false)
    private Float afterWeight;
    @Column(nullable = false)
    private LocalDate signUpDate;

}
