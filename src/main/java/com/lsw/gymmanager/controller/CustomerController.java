package com.lsw.gymmanager.controller;

import com.lsw.gymmanager.model.CustomerItem;
import com.lsw.gymmanager.model.CustomerRequest;
import com.lsw.gymmanager.model.CustomerResponse;
import com.lsw.gymmanager.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class CustomerController {

    private final CustomerService customerService;

    @PostMapping("/data")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return "ok";

    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }

    @GetMapping("/data/id/{id}")
    public CustomerResponse getCustomer(@PathVariable long id) {
        CustomerResponse result = customerService.getCustomer(id);
        return result;
    }

}
