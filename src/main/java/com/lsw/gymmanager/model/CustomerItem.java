package com.lsw.gymmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerItem {

    private Long id;

    private String name;

    private Float age;

    private Float height;

    private Float beforeWeight;

    private Float afterWeight;

    private LocalDate signUpDate;

}
