package com.lsw.gymmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CustomerResponse {
    private Long id;
    private String name;
    private Float age;
    private Float height;
    private String weight;
    private LocalDate signUpDate;


}
