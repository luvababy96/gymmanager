package com.lsw.gymmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;


@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String name;

    @NotNull
    private Float age;
    @NotNull
    private Float height;
    @NotNull
    private Float beforeWeight;
    @NotNull
    private Float afterWeight;


}
